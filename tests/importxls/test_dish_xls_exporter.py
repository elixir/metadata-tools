import os

from pathlib import Path
from unittest import TestCase

from metadata_tools.importxls.export_utils import save_exported_datasets_to_file
from metadata_tools.importxls.dish_xls_exporter import DishXlsExporter

from jsonschema import validate
from jsonschema.exceptions import SchemaError, ValidationError
from json import loads
import pyexcel

from metadata_tools.importxls.validators import DISHVersionValidator

class TestDishXlsExporter(TestCase):
    def test_dish_export(self):

        def process_excel_file(fname):
            try:
                full_file_path = os.path.join(dirName, fname)
                dataset_dict = exporter.export_submission(full_file_path)
                with open(Path(full_file_path).stem + ".json", 'w') as outfile:
                    save_exported_datasets_to_file(dataset_dict, outfile)
            except ValueError:
                    self.fail("Could not import {}".format(full_file_path))
            return dataset_dict

        def validate_dataset_dict(dataset_dict):
            schemaFile = open("metadata_tools/resources/json-schemas/schemas/elu-dataset.json", encoding='utf-8')
            schema = loads(schemaFile.read())
            try:
                validate(dataset_dict, schema)
            except ValidationError:
                self.fail(f"Validation of '{fname}' failed using schema 'json-schemas/schemas/elu-dataset.json'")
            finally:
                schemaFile.close()

            return

        exporter = DishXlsExporter()

        for dirName, _, fileList in os.walk(os.path.join(os.path.dirname(__file__), 'resources')):
            for fname in fileList:
                if fname.lower().endswith('xls') or fname.lower().endswith('xlsx'):
                    # Test export from Excel to JSON
                    dataset_dict = process_excel_file(fname)

                    # Validate resulting JSON file
                    validate_dataset_dict(dataset_dict)

        return
    
    def test_version_validator(self):
        test_book = pyexcel.Book()
        test_book += pyexcel.Sheet(name="_Dictionary")
        test_book[0][0, 0] = 'v.1213124121231231231'
        
        version_validator = DISHVersionValidator()
        
        with self.assertRaises(NotImplementedError):
            version_validator.validate_book(test_book)

        supported_version = version_validator.get_supported_version()
        test_book[0][0, 0] = supported_version
        
        self.assertTrue(version_validator.validate_book(test_book))
        return

